#!/usr/bin/python2
import BaseHTTPServer
import SimpleHTTPServer
import sys
import base64
import subprocess
import os
import ssl
from SocketServer import ThreadingMixIn
from threading import Thread
from time import sleep

username = "martin"
passwd = "jdunato"
secureport = 40100 
publicport = 40101
scriptdir = os.path.dirname(os.path.realpath(__file__))
publicdir = scriptdir+'/public'
certdir = scriptdir+'/certs'
camdir = scriptdir+'/cam'

class AuthHandler(ThreadingMixIn, SimpleHTTPServer.SimpleHTTPRequestHandler):
    ''' Main class to present webpages and authentication. '''
    def do_HEAD(self):
        print "send header"
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_AUTHHEAD(self):
        print "send header"
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm=\"Camera Server\"')
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        global key
        ''' Present frontpage with user authentication. '''
        if self.headers.getheader('Authorization') == None:
            self.do_AUTHHEAD()
            self.wfile.write('no auth header received')
            pass
        elif self.headers.getheader('Authorization') == 'Basic '+base64.b64encode(username+':'+passwd):
            cmd = "cd "+camdir+" && fswebcam -r 1280x960 photo.jpg"
            subprocess.call(cmd, shell=True) # Do it twice because first image has bad quality
            subprocess.call(cmd, shell=True)
            os.chdir(camdir)
            SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)
            pass
        else:
            self.do_AUTHHEAD()
            self.wfile.write(self.headers.getheader('Authorization'))
            self.wfile.write('not authenticated')
            pass

class PublicHandler(ThreadingMixIn, SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):
        os.chdir(publicdir)
        SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

secureHttpd = BaseHTTPServer.HTTPServer(('', secureport), AuthHandler)
secureHttpd.socket = ssl.wrap_socket(secureHttpd.socket, certfile=certdir+'/server.crt', server_side=True, keyfile=certdir+'/server.key')
print "Starting secure server..."
Thread(target=secureHttpd.serve_forever).start()

publicHandler = SimpleHTTPServer.SimpleHTTPRequestHandler
publicHttpd = BaseHTTPServer.HTTPServer(('', publicport), PublicHandler)
print "Starting public server..."
Thread(target=publicHttpd.serve_forever).start()

while True:
    sleep(60)
