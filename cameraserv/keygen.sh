#!/bin/bash
if [ "$1" == "" ]; then
  echo "$0 domainname.no-ip.com"
  exit
else
  domain=$1
fi
mkdir -p certs
cd certs
openssl genrsa -out rootCA.key 4096
openssl req -x509 -new -nodes -key rootCA.key -days 3650 -out rootCA.pem \
  -subj "/C=/ST=Y/L=/O=Raspberry Pi/OU=/CN=$domain"
openssl genrsa -des3 -passout pass:x -out server.pass.key 2048
openssl rsa -passin pass:x -in server.pass.key -out server.key
rm server.pass.key
openssl req -new -key server.key -out server.csr \
  -subj "/C=/ST=Y/L=/O=Raspberry Pi/OU=/CN=$domain"
openssl x509 -req -days 3650 -in server.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out server.crt
openssl x509 -in rootCA.pem -outform der -out rootCA.der.crt
cd ..
mkdir -p public
cp certs/rootCA.der.crt public/rootCA.der.crt
